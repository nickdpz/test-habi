import boto3
import os
import json


bucket_name = 'test-pro-images-bucket'
file_name = 'habi.png'

def lambda_handler(event, context):
    print('Intro lambda')
    s3 = boto3.client('s3')
    key_name = file_name
    with open(os.path.join("src", file_name), 'rb') as data:
        s3.upload_fileobj(data, bucket_name, key_name)
    print('Upload file')
    result = {
        "statusCode": 200,
        "headers": {
			"Content-Type": "application/json",
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "OPTIONS,POST,GET",
			"Access-Control-Allow-Headers": "Content-Type",
		},
        "body": json.dumps({"message":"uploaded image"})
    }
    print('Result', result)
    return result
